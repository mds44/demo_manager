<?php

class Project extends CI_Controller {

	var $project_path = 'p';
	var $projects_file = 'projects.json';
	var $upload_path = 'uploads';
	var $projects = array();

	public function __construct()
	{
		parent::__construct();
		$this->update_projects();
	}

	private function update_projects()
	{
		$this->open_projects();
		$this->load->vars('projects', $this->projects);
		$this->load->vars('contents', get_dir_file_info($this->project_path, true));
	}

	public function index()
	{
		$this->load->vars('contents', get_dir_file_info($this->project_path, true));
		$this->load->view('projects/index');
	}

	public function store()
	{
		if ($this->validate_form()) {
			$projectFile = $this->upload_project('project_file');
			$this->extract_project($projectFile);
			$this->save_project();
			$this->clean_up($projectFile);
			$this->update_projects();
			$this->load->view('projects/index', array(
				'success' => 'Project has been added.'
			));
		}
	}

	private function validate_form()
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules('project_name', 'Project Name', 'required|callback_validate_name');
		$this->form_validation->set_rules('file_name', 'Launch File Name', 'required|callback_validate_file');

		if ( ! $this->form_validation->run()) {
			$this->load->view('projects/index', array(
				'errors' => validation_errors()
			));
			return false;
		}

		return true;
	}

	public function validate_name($value)
	{
		if (preg_match('/^[0-9a-z_-]+$/i', $value)) {
			return true;
		}

		$this->form_validation->set_message('validate_name', 'The %s field can only contain letters, numbers, underscores and hyphens.');
		return false;
	}

	public function validate_file($value)
	{
		if (preg_match('/^[a-z0-9_-]+(.html|.htm)$/i', $value)) {
			return true;
		}

		$this->form_validation->set_message('validate_file', 'The %s field can only accept files ending ".html" or ".htm" and contains no spaces.');
		return false;
	}

	private function upload_project($fileName)
	{
		$this->load->library('upload', array(
			'upload_path' => $this->upload_path,
			'allowed_types' => 'zip'
		));

		if ( ! $this->upload->do_upload($fileName)) {
			$this->load->view('projects/index', array(
				'errors' => $this->upload->display_errors()
			));
		}

		return $this->upload->data();
	}

	private function extract_project($file)
	{
		$zip = new ZipArchive();

		if ($zip->open($this->upload_path . '/' . $file['file_name'])) {
			$zip->extractTo(
				$this->project_path . '/' .
					$this->input->post('project_name') . '/'
			);
			$zip->close();
			return true;
		}

		return false;
	}

	private function open_projects()
	{
		$this->projects = (array) json_decode(read_file($this->project_path . '/' . $this->projects_file));
	}

	private function save_project()
	{
		$this->projects[$this->input->post('project_name')] = $this->input->post('file_name');
		return $this->save_projects();
	}

	private function save_projects()
	{
		if ( ! $this->projects) return false;

		if (write_file($this->project_path . '/' . $this->projects_file, json_encode($this->projects))) {
			return true;
		}

		return false;
	}

	private function clean_up($file)
	{
		unlink($this->upload_path . '/' . $file['file_name']);
	}

	public function delete($name)
	{
		// Remove from JSON.
		unset($this->projects[$name]);
		$this->save_projects();

		// Remove from file system
		$dir = $this->project_path . '/' . $name;
		delete_files($dir, true);
		rmdir($dir);

		redirect(base_url(), 'refresh');
	}

}

/* End of Project controller. */
