<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Demo Manager</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/style.css">
</head>
<body>
	<h1><a href="/demo/">Demo Manager</a></h1>

	<?php echo form_open_multipart(site_url() . "/project/store"); ?>

		<h2>Add a Project</h2>
		<p>Please assure that your launch file is in the root of the zip file. If you use the same application folder as one that already exists. It will be overwritten.</p>

		<?php if (isset($errors)) : ?><div class="error"><?php echo $errors; ?></div><?php endif; ?>
		<?php if (isset($success)) : ?><div class="success"><?php echo $success . " "; ?><a href="<?php echo base_url(); ?>p/<?php echo $_POST['project_name'];?>/<?php echo $_POST['file_name'];?>">View Now</a></div><?php endif; ?>

		<label for="project_name">Project Name</label>
		<input type="text" name="project_name" id="project_name">

		<label for="file_name">Launch File Name</label>
		<input type="text" name="file_name" id="file_name"
			value="index.html"
		>

		<label for="project_file">Zip File</label>
		<input type="file" name="project_file">

		<button type="submit">Add</button>

	<?php echo form_close(); ?>

	<?php unset($contents['projects.json']); ?>

	<?php 

		//Sorting by date, so most recent upload appears at the top of the table
		usort($contents, function($a, $b) 
		{
    		return $b['date'] - $a['date'];
		});

	?>

	<h2>Projects</h2>
	<table>
		<thead>
			<tr>
				<th class="width60">Name</th>
				<th>Date</th>
				<th class="center">Delete</th>
			</tr>
		</thead>
		<?php foreach ($contents as $content) : ?>
			<tbody>
				<tr>
					<td><a href="<?php echo base_url(); ?>p/<?php echo $content['name']; ?>/<?php echo $projects[$content['name']]; ?>"><?php echo $content['name']; ?></a></td>
					<td><?php echo date('m/d/Y', $content['date']); ?></td>
					<td class="center"><a href="<?php echo site_url(); ?>/project/delete/<?php echo $content['name']; ?>">X</a></td>
				</tr>
			</tbody>
		<?php endforeach; ?>
	</table>
</body>
</html>
